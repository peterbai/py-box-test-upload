import requests
import json
import sys
import os


def get_folder_contents(folderid):
    # Warning: this does not yet handle folders with more than 1000 items

    url = 'https://api.box.com/2.0/folders/' + str(folderid) + '/items'
    payload = {
        'limit': 1000
    }
    headers = {
        'Authorization': 'Bearer ' + token
    }

    r = requests.get(url, headers = headers, params=payload)

    if '401' in str(r.status_code):
        print 'Error: 401 Unauthorized. Check your access token. Exiting.'
        exit(1)
    elif r.text == '':
        print 'Unexpected response from server. Exiting.'
        exit(1)

    response = json.loads(r.text)
    # print json.dumps(response, indent=4)

    return response

def get_input_file_directory(filename):

    __location__ = os.path.realpath(os.path.join(
        os.getcwd(),
        os.path.dirname(__file__))
        )

    inputFile = __location__ + '/files/' + filename

    return inputFile

def upload_file(filename):

    filePath = get_input_file_directory(filename)

    print 'uploading: ' + filePath

    url = 'https://upload.box.com/api/2.0/files/content'
    payload = {'parent_id': 0}
    files = {'file': open(filePath, 'rb')}
    headers = {
        'Authorization': 'Bearer ' + token,
        'As-User': 219612613
    }

    r = requests.post(url, headers=headers, data=payload, files=files)

    print r.status_code
    print r.text
    print r.headers


token = os.environ.get('BOX_ACCESS_TOKEN')
if token == None:
    print 'No token defined in environment. Exiting.'
    exit(1)

if (len(sys.argv) == 2):
    file_to_upload = sys.argv[1]
else:
    print 'No filename specified. Exiting.'
    exit()

upload_file(file_to_upload)